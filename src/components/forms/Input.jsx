import "./Input.css";

import React, { useState } from "react";

export default (props) => {
  const [valor, setValor] = useState("Inicial");

  function Changed(e) {
    setValor(e.target.value);
  }

  return (
    <div className="Input">
      <h2>{valor}</h2>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <input value={valor} onChange={Changed}/>
        <input value={undefined}/>
      </div>
    </div>
  );
};
