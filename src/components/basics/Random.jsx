import React from "react";

export default (props) => {
  const { max, min } = props;
  const rng = Math.floor(Math.random() * (max - min)) + min;

  return (
    <div>
      <h2>Valor Aleatório</h2>
      <p>
        <strong>Valor Máx:</strong> {max}
      </p>
      <p>
        <strong>Valor Min:</strong> {min}
      </p>
      <p>
        <strong>Valor Gerado: </strong> {rng}
      </p>
    </div>
  );
};
