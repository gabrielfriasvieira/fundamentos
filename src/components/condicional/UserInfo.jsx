import React from "react";
import If, {Else} from "./if";

export default (props) => {
  const user = props.user || {};
  return (
    <div>
      <If test={user && user.name}>
        Seja bem Vindo <strong>{user.name}</strong>
        <Else>
          Seja bem Vindo <strong>Amigao</strong>
        </Else>
      </If>
    </div>
  );
};
