import "./tableProducts.css";

import React from "react";
import listProducts from "../../data/product";
import product from "../../data/product";

export default (props) => {
  const el = listProducts.map((listProduct, i) => {
    return (
      <tr key={listProduct.id} className={i % 2 === 0 ? "Par" : "Impar"}>
        <td>{listProduct.id})</td>
        <td>{listProduct.nome}</td>
        <td>R${listProduct.price.toFixed(2).replace(".", ",")} reais.</td>
      </tr>
    );
  });

  return (
    <div className="TableProducts">
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Valor</th>
          </tr>
        </thead>
        <tbody>{el}</tbody>
      </table>
    </div>
  );
};
