import "./Mega.css";
import React, { useState } from "react";

export default (props) => {
  function randomGerator(max, min, array) {
    const random = parseInt(Math.random() * (max + 1 - min)) + min;

    return array.includes(random) ? randomGerator(max, min, array) : random;
  }

  function numberGerator(qtde) {
    const number = Array(qtde)
      .fill(0)
      .reduce((nums) => {
        const newNumber = randomGerator(1, 60, nums);
        return [...nums, newNumber];
      }, [])
      .sort((n1, n2) => n1 - n2);

    return number;
  }

  const [qtde, setQtde] = useState(props.qtde || 6);
  const numberStart = numberGerator(qtde);
  const [number, setNumbers] = useState(numberStart);

  return (
    <div className="Mega">
      <h2>Mega Sena</h2>
      <h3>{number.join(" ")}</h3>
      <div>
        <label>Qtde de Números</label>
        <input
          type="number"
          value={qtde}
          onChange={(e) => {
            setQtde(+e.target.value);
            setNumbers(numberGerator(+e.target.value));
          }}
        />
      </div>
      <button onClick={(_) => setNumbers(numberGerator(qtde))}>Gerar</button>
    </div>
  );
};
