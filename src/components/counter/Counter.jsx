import "./Counter.css";
import React, { Component } from "react";

import Button from "./Button";
import Display from "./Display";
import StepForm from "./StepForm";

class Counter extends Component {
  state = {
    step: this.props.stepValor || 0,
    numb: this.props.number || 5,
  };

  inc = () => {
    this.setState({
      numb: this.state.numb + this.state.step,
    });
  };

  dec = () => {
    this.setState({
      numb: this.state.numb - this.state.step,
    });
  };

  setStep = (newStep) => {
    this.setState({
      step: newStep,
    });
  };

  render() {
    return (
      <div className="Counter">
        <h2>Contador</h2>
        <Display numb={this.state.numb} />
        <StepForm step={this.state.step} setStep={this.setStep} />
        <Button setDec={this.dec} setInc={this.inc} />
      </div>
    );
  }
}

export default Counter;
