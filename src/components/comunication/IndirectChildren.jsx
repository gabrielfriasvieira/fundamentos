import React from "react";

export default (props) => {
  const cb = props.actionClick;
  const gerarIdade = () => parseInt(Math.random() * 20 + 50);
  const gerarNota = () => Math.random > 0.5;

  return (
    <div>
      <div>Filho</div>
      <button onClick={(e) => cb("João", gerarIdade, gerarNota)}>
        Fornecer Informações
      </button>
    </div>
  );
};
