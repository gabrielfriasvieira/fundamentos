import React from "react";
import DirectChildren from "./DirectChildren";

export default (props) => {
  return (
    <div>
      <DirectChildren text="Gabriel" age={20} bool={true} />
      <DirectChildren text="Lais" age={18} bool={false} />
    </div>
  );
};
