import "./App.css";
import React from "react";

import Mega from "./components/mega/Mega";
import Counter from "./components/counter/Counter";
import Input from "./components/forms/Input";
import IndirectFather from "./components/comunication/IndirectFather";
import DirectFather from "./components/comunication/DirectFather";
import UserInfo from "./components/condicional/UserInfo";
import ParOuImpart from "./components/condicional/ParOuImpart";
import ListProducts from "./components/repeat/tableProducts";
import ListStudent from "./components/repeat/ListStudent";
import FamilyMember from "./components/basics/FamilyMember";
import Family from "./components/basics/Family";
import Card from "./components/layout/Card";
import First from "./components/basics/First";
import ComParameter from "./components/basics/ComParameter";
import Fragment from "./components/basics/Fragment";
import Random from "./components/basics/Random";

export default () => (
  <div className="App">
    <h1>Fundamentos React 2</h1>

    <div className="Cards">
      <Card titulo="#13 - Mega Sena" color="#461582">
        <Mega max={70} min={0}/>
      </Card>

      <Card titulo="#12 - Contador" color="#424242">
        <Counter number={10} stepValor={10}></Counter>
        <Counter number={20} stepValor={5}></Counter>
      </Card>

      <Card titulo="#11 - Componente Controlado" color="#E45F56">
        <Input></Input>
      </Card>

      <Card titulo="#10 - Comunicação Indireta" color="#8BAD39">
        <IndirectFather></IndirectFather>
      </Card>

      <Card titulo="#9 - Comunicação Direta" color="#59323C">
        <DirectFather></DirectFather>
      </Card>

      <Card titulo="#8 - Renderizar Condicional" color="#982395">
        <ParOuImpart number={20}></ParOuImpart>
        <UserInfo user={{ name: "Gabriel" }}></UserInfo>
        <UserInfo user={{ email: "ga@pme.com" }}></UserInfo>
      </Card>

      <Card titulo="#7 Repetição - Lista de Compras" color="#3A9AD9">
        <ListProducts></ListProducts>
      </Card>

      <Card titulo="#6 Repetição" color="#FF4C65">
        <ListStudent></ListStudent>
      </Card>

      <Card titulo="#5 Componente com Filhos" color="#00C8F8">
        <Family sobrenome="Ferreira">
          <FamilyMember nome="Pedro"></FamilyMember>
          <FamilyMember nome="Ana"></FamilyMember>
          <FamilyMember nome="Gustavo"></FamilyMember>
        </Family>
      </Card>

      <Card titulo="#4 Desafio Aleatório" color="#FA6900">
        <Random max={100} min={50}></Random>
      </Card>

      <Card titulo="#3 Fragmento" color="#E94C6F">
        <Fragment />
      </Card>

      <Card titulo="#2 Com Parâmetro" color="#E8B71A">
        <ComParameter titulo="Situação do Aluno" aluno="Pedro" nota={9.3} />
      </Card>

      <Card titulo="#1 Primeiro Componente" color="#588C73">
        <First></First>
      </Card>
    </div>
  </div>
);
